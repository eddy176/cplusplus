#ifndef PPM_H_
#define PPM_H_
#include <vector>
#include <string>


class PPM
{
	public:
		PPM();
		
		int getHeight() const;
		int getWidth() const;
		int getMaxColorValue() const;
		int getChannel( const int& row, const int& column, const int& channel) const;
		void setHeight( const int& height );
		void setWidth( const int& width );
		void setMaxColorValue( const int& max_color_value );
		void setChannel( const int& row, const int& column, const int& channel, const int& value );
		
		bool operator>( const PPM& p) const;
		bool operator==( const PPM& p) const;
		bool operator!=( const PPM& p) const;
		bool operator<( const PPM& p) const;
		bool operator<=( const PPM& p) const;
		bool operator>=( const PPM& p) const;
		

		PPM& operator+=( const PPM& rhs);
		PPM& operator-=( const PPM& rhs);
		PPM operator+( const PPM& rhs) const;
		PPM operator-( const PPM& rhs) const;
		PPM& operator*=( const double& rhs);
		PPM& operator/=( const double& rhs);
		PPM operator*( const double& rhs) const;
		PPM operator/( const double& rhs) const;
		PPM copy(const PPM& p);
		PPM red(const PPM& p);
		PPM blue(const PPM& p);
		PPM green(const PPM& p);
		PPM linear(const PPM& p);
		PPM vertical(const PPM& p);
		PPM horizontal(const PPM& p);
		void sepiaFilter( PPM& dst ) const;



	protected:
		std::vector<unsigned char> mChannels;
		int mWidth, mHeight, mMaxColorValue;

};		

PPM createTestPattern( const int width, const int height, const int max_color_value );
std::istream& operator>>(std::istream& is, PPM& p);
std::ostream& operator<<(std::ostream& os, const PPM& p);	


#endif
