#ifndef JULIASET_H_
#define JULIASET_H_
#include <vector>
#include <string>

#include"ComplexFractal.h"

class JuliaSet : public ComplexFractal
{
    
    public:
        JuliaSet();
        virtual ~JuliaSet( );
 
        double getA() const;
        double getB() const;

        void setParameters( const double& a, const double& b );
        void calculateNextPoint( const double x0, const double y0, double& x1, double &y1 ) const; 
        int calculatePlaneEscapeCount( const double& x0, const double& y0 ) const;
        int calculatePixelEscapeCount( const int& row, const int& column ) const;
        void findInterestingPoint( double& xc, double& yc ) const;
        void setRandomCenter( const double& xsize, const double& ysize );
        

        
    protected:
        double mA, mB;

};


#endif

