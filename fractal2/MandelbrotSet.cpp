#include "MandelbrotSet.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <string>

MandelbrotSet::MandelbrotSet( )
    : JuliaSet() {

}

MandelbrotSet::~MandelbrotSet( )
{

}

void MandelbrotSet::calculateNextPoint( const double x0, const double y0, const double& a, const double& b, double& x1, double &y1 ) const
{
    x1 = x0*x0 - y0*y0 + a;
    y1 = 2*x0*y0 + b;
}

int MandelbrotSet::calculatePlaneEscapeCount( const double& a, const double& b ) const
{
    int iters = 0;
    double x0 = 0, y0 = 0;
    double distance, x1, y1;
    double xc = x0, yc = y0;
    calculateNextPoint(xc, yc, a, b, x1, y1);
    distance = x1*x1 + y1*y1;
    if( distance > 4 )
    {
        return iters;
    }
    while( iters <= this->mMaxEscapeCount - 1 )
    {
        distance = x1*x1 + y1*y1;
        if( distance > 4 )
        {
            return iters;
        }
        iters += 1;
        xc = x1;
        yc = y1;
        calculateNextPoint(xc, yc, a, b, x1, y1);

        
    }
    return iters;
}

int MandelbrotSet::calculatePixelEscapeCount( const int& row, const int& column ) const
{
    int iters = 0;
    double x0, y0, xc, yc;
    if ( row >= 0 && row <= mHeight -1 && column >= 0 && column <= mWidth -1 )
    {
        x0 = calculatePlaneXFromPixelColumn( column );
        y0 = calculatePlaneYFromPixelRow( row );
        xc = x0;
        yc = y0;
        iters = calculatePlaneEscapeCount( xc, yc );
        return iters;

    }
    return -1;

}

MandelbarSet::MandelbarSet( )
    : MandelbrotSet() {

}

MandelbarSet::~MandelbarSet( )
{

}
void MandelbarSet::calculateNextPoint( const double x0, const double y0, const double& a, const double& b, double& x1, double &y1 ) const
{
    x1 = x0*x0 - y0*y0 + a;
    y1 = -2*x0*y0 + b;
}