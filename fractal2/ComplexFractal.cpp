#include "ComplexFractal.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <string>

ComplexFractal::ComplexFractal()
    :  mEscapeCounts(120000) {

    this->mWidth = 400;
    this->mHeight = 300;
    this->mMaxEscapeCount = 255;
    this->mMaxX = 2.0;
    this->mMinX = -2.0;
    this->mMaxY = 1.5;
    this->mMinY = -1.5;
    this->mDeltaX = 0.01;
    this->mDeltaY = 0.01;
}

ComplexFractal::~ComplexFractal( )
{

}

int ComplexFractal::getWidth( ) const
{
    return this->mWidth;
}

int ComplexFractal::getHeight( ) const
{
    return this->mHeight;
}

double ComplexFractal::getMinX( ) const
{
    return this->mMinX;
}

double ComplexFractal::getMaxX( ) const
{
    return this->mMaxX;
}

double ComplexFractal::getMinY( ) const
{
    return this->mMinY;
}

double ComplexFractal::getMaxY( ) const
{
    return this->mMaxY;
}

int ComplexFractal::getMaxEscapeCount( ) const
{
    return this->mMaxEscapeCount;
}

void ComplexFractal::setPixelSize( const int& width, const int& height )
{
    double min_x = this->mMinX;
    double max_x = this->mMaxX;
    double min_y = this->mMinY;
    double max_y = this->mMaxY;
    double gapx, gapy;
    int counts;
    if (width >= 2 && height >= 2)
    {
        this->mWidth = width;
        this->mHeight = height;

        gapx = ( max_x - min_x ) / ( width - 1);
        this->mDeltaX = gapx;

        gapy = ( max_y - min_y ) / ( height - 1 );
        this->mDeltaY = gapy;
        counts = width * height;
        mEscapeCounts.resize(counts);
    }
}

void ComplexFractal::setPlaneSize( const double& min_x, const double& max_x, const double& min_y, const double& max_y )
{
    int height = this->mHeight;
    int width = this->mWidth;
    double gapx, gapy;
    if( min_x >= -2.0 && min_x <= 2.0 )
    {
        if( max_x >= -2.0 && max_x <= 2.0 )
        {
            if( min_y <= 2.0 && min_y >= -2.0 )
            {
                if( max_y >= -2.0 && max_y <= 2.0 )
                {
                    if( max_x != min_x )
                    {
                        if( max_y != min_y )
                        {
                            if( min_x > max_x )
                            {
                                this->mMaxX = min_x;
                                this->mMinX = max_x;

                                gapx = ( max_x - min_x ) / ( width - 1);
                                this->mDeltaX = gapx;
                            }
                            else
                            {
                                this->mMaxX = max_x;
                                this->mMinX = min_x;

                                gapx = ( max_x - min_x ) / ( width - 1);
                                this->mDeltaX = gapx;
                            }

                            if( min_y > max_y )
                            {
                                this->mMaxY = min_y;
                                this->mMinY = max_y;

                                gapy = ( max_y - min_y ) / ( height - 1 );
                                this->mDeltaY = gapy;

                            }
                            else
                            {
                                this->mMaxY = max_y;
                                this->mMinY = min_y;

                                gapy = ( max_y - min_y ) / ( height - 1 );
                                this->mDeltaY = gapy;
                            }
                        }
                    }
                }
            }
        }

    }
}

void ComplexFractal::setMaxEscapeCount( const int& count )
{
    if (count >= 0)
    {
        this->mMaxEscapeCount = count;
    }
}

double ComplexFractal::getDeltaX( ) const
{
    return this->mDeltaX;
}

double ComplexFractal::getDeltaY( ) const
{
    return this->mDeltaY;
}

void ComplexFractal::setDeltas( const double& delta_x, const double& delta_y )
{
    if( delta_x >= 0 && delta_y >= 0 )
    {
        this->mDeltaX = delta_x;
        this->mDeltaY = delta_y;
    }
}

double ComplexFractal::calculateDeltaX( ) const
{
    double delta_x, min, max;
    int width;
    max = this->mMaxX;
    min = this->mMinX;
    width = this->mWidth;
    delta_x = (max - min) / (width - 1);
    return delta_x;
}

double ComplexFractal::calculateDeltaY( ) const
{
    double delta_y, min, max;
    int height;
    max = this->mMaxY;
    min = this->mMinY;
    height = this->mHeight;
    delta_y = ( max - min ) / ( height - 1 );
    return delta_y;
}

double ComplexFractal::calculatePlaneXFromPixelColumn( const int& column ) const
{
    double delta_x, min, max, x;
    int width = this->mWidth;
    max = this->mMaxX;
    min = this->mMinX;
    delta_x = ( max - min ) / ( width - 1 );
    if( column >= 0 && column <= width - 1 )
    {
        x = column *  delta_x + min;
        return x;
    }
    return 0;
}

double ComplexFractal::calculatePlaneYFromPixelRow( const int& row ) const
{
    int height = this->mHeight;
    double delta_y, min, max, y;
    max = this->mMaxY;
    min = this->mMinY;
    delta_y = ( max - min ) / ( height - 1 );
    if( row >= 0 && row <= height - 1 )
    {
        y = max - row * delta_y;
        return y;
    }
    return 0;
}

void ComplexFractal::calculatePlaneCoordinatesFromPixelCoordinates( const int& row, const int& column, double& x, double& y ) const
{

    if( column >= 0 && column <= mWidth - 1 && row >= 0 && row  <= mHeight - 1 )
    {
        x = calculatePlaneXFromPixelColumn( column );
        y = calculatePlaneYFromPixelRow( row );
    }
    else
    {
        x = 0;
        y = 0;
    }
}

const std::vector< int >& ComplexFractal::getEscapeCounts( ) const
{
    return this->mEscapeCounts;
}

int ComplexFractal::getEscapeCount( const int& row, const int& column ) const
{

    int index;
    if( column >= 0 && column <= mWidth - 1 && row >= 0 && row  <= mHeight - 1 )
    {
        index = row * mWidth + column;
        return mEscapeCounts[index];

    }
    return -1;
}

void ComplexFractal::setEscapeCount( const int& row, const int& column, const int& count )
{
    int index;
    if( column >= 0 && column <= mWidth - 1 && row >= 0 && row  <= mHeight - 1 && count <= mMaxEscapeCount )
    {
        index = row * mWidth + column;
        mEscapeCounts[index] = count;
    }
}

void ComplexFractal::calculateAllEscapeCounts( )
{
    int i, j, count;
    for( i = 0; i <= mWidth - 1; i ++ )
    {
        for( j = 0; j <= mHeight - 1; j++ )
        {
            count = calculatePixelEscapeCount(j, i );
            setEscapeCount(j, i, count);
        }
    }
}

void ComplexFractal::setPPM( PPM& ppm ) const
{
    int height = mHeight, width = mWidth, mcv = 63;
    int i, j, count;
    ppm.setHeight(height);
    ppm.setWidth(width);
    ppm.setMaxColorValue(mcv);
    
    for( j = 0; j <= height; j++ )
    {
        for( i = 0; i <= width; i++ )
        {
            
            count = getEscapeCount(j, i);
            if( count == 0 || count % 8 == 4)
            {
                ppm.setChannel(j,i,0,0);
                ppm.setChannel(j,i,1,0);
                ppm.setChannel(j,i,2,0);
            }
            else if( count == mMaxEscapeCount || count % 8 == 1 )
            {
                ppm.setChannel(j,i,0,63);
                ppm.setChannel(j,i,1,31);
                ppm.setChannel(j,i,2,31);
            }
            else if( count % 8 == 0)
            {
                ppm.setChannel(j,i,0,63);
                ppm.setChannel(j,i,1,63);
                ppm.setChannel(j,i,2,63);
            }
            else if( count % 8 == 2)
            {
                ppm.setChannel(j,i,0,63);
                ppm.setChannel(j,i,1,63);
                ppm.setChannel(j,i,2,31);
            }
            else if( count % 8 == 3 )
            {
                ppm.setChannel(j,i,0,31);
                ppm.setChannel(j,i,1,63);
                ppm.setChannel(j,i,2,31);
            }
            else if( count % 8 == 5 )
            {
                ppm.setChannel(j,i,0,31);
                ppm.setChannel(j,i,1,63);
                ppm.setChannel(j,i,2,63);
            }
            else if( count % 8 == 6 )
            {
                ppm.setChannel(j,i,0,31);
                ppm.setChannel(j,i,1,31);
                ppm.setChannel(j,i,2,63);
            }
            else if( count % 8 == 7 )
            {
                ppm.setChannel(j,i,0,63);
                ppm.setChannel(j,i,1,31);
                ppm.setChannel(j,i,2,63);
            }
        }
    }
}
void ComplexFractal::setPlaneFromCenter( const double& xc, const double& yc, const double& xsize, const double& ysize )
{
    double minx_v, maxx_v, maxy_v,miny_v ;
    if( xsize > 0 && ysize > 0)
    {   
        minx_v = xc - xsize / 2;
        if( minx_v < -2.0)
        {
            minx_v = -2.0;
        }
        maxx_v = xc + xsize / 2;
        if( maxx_v > 2.0)
        {
            maxx_v = 2.0;
        }

 
        miny_v = yc - ysize / 2;
        if( miny_v < -2.0)
        {
            miny_v = -2.0;
        }
        maxy_v = yc + ysize / 2;
        if( maxy_v > 2.0)
        {
            maxy_v = 2.0;
        }
        setPlaneSize( minx_v, maxx_v, miny_v, maxy_v );
    }
}

void ComplexFractal::setPPM( PPM& ppm, const ColorTable& colors ) const
{
     if( colors.getNumberOfColors() >= 3 )
    {
        int height = mHeight, width = mWidth;
        int i, j, count;
        int mcv = 0;
        std::cout << mcv << std::endl;

        for( unsigned int m = 0; m <= colors.getNumberOfColors(); m++ )
        {
            if( colors[m].getRed() > mcv )
            {
                mcv = colors[m].getRed();
                
            }
            if( colors[m].getGreen() > mcv ) 
            {
                mcv = colors[m].getGreen();
                std::cout << mcv << std::endl;
            }
            if( colors[m].getBlue() > mcv )
            {
                mcv = colors[m].getBlue();
                std::cout << mcv << std::endl;
            } 
        }
        ppm.setHeight(height);
        ppm.setWidth(width);
        ppm.setMaxColorValue(mcv);


        for( j = 0; j <= height; j++ )
        {
            for( i = 0; i <= width; i++ )
            {
                
                count = getEscapeCount(j, i);
                if( count == mMaxEscapeCount )
                {
                    int index = colors.getNumberOfColors() - 1;
                    ppm.setChannel(j,i,0,colors[index].getRed() );
                    ppm.setChannel(j,i,1,colors[index].getGreen() );
                    ppm.setChannel(j,i,2,colors[index].getBlue() );
                }
                else if( count == 0 )
                {
                    int index = colors.getNumberOfColors() - 2;
                    ppm.setChannel(j,i,0,colors[index].getRed() );
                    ppm.setChannel(j,i,1,colors[index].getGreen() );
                    ppm.setChannel(j,i,2,colors[index].getBlue() );
                }
                else if( count != mMaxEscapeCount && count != 0 )
                {
                    int index = (count % (colors.getNumberOfColors() - 2));
                    ppm.setChannel(j,i,0,colors[index].getRed() );
                    ppm.setChannel(j,i,1,colors[index].getGreen() );
                    ppm.setChannel(j,i,2,colors[index].getBlue() );
                }
            }
        }
    }
}

