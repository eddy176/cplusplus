#include "ppm.h"
#include <iostream>
#include <fstream>
#include <string>		


int main()
{
	PPM ppm1;
	PPM ppm2;
	PPM ppm3;
	char filename1[64];
	char filename2[64];

	std::string answer;
	std::cout << "Conversions available" << std::endl; 
	std::cout << "C) Copy directly." << std::endl;
	std::cout << "r) Grayscale from red." << std::endl;
	std::cout << "g) Grayscale from green." << std::endl;
	std::cout << "b) Grayscale from blue." << std::endl;
	std::cout << "l) Grayscale from linear colorimetric." << std::endl;
	std::cout << "v) Vertical edge detection" << std::endl;
	std::cout << "h) Horizontal edge detection" << std::endl;
	std::cout << "Choice?" << std::endl;

	std::cin >> answer;

	std::cout << "Input filename?" << std::endl;
	std::cin >> filename1;

	std::ifstream myfile (filename1);
	if (myfile.is_open())
  	{
  		myfile >> ppm1;
  	}
	
	if (answer == "C")
	{
		ppm2.copy(ppm1);
	}

	else if (answer == "r")
	{
		
		ppm2.red(ppm1);
	}
	else if (answer == "b")
	{
		
		ppm2.blue(ppm1);
	}
	else if (answer == "g")
	{
		
		ppm2.green(ppm1);
	}
	else if (answer == "l")
	{
		
		ppm2.linear(ppm1);
	}
	else if (answer == "v")
	{
		
		ppm2.vertical(ppm1);
	}
	else if (answer == "h")
	{
		
		ppm2.horizontal(ppm1);
	}

	myfile.close();
	char newfile[64];
	std::cout << "Output filename?" << std::endl;
	std::cin >> newfile;

	std::ofstream outfile (newfile);
	if (outfile.is_open())
	{
		outfile << ppm2;
	}
	outfile.close();

	return 0;

}