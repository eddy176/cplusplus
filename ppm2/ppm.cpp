#include "ppm.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <string>
//linear grayscale


PPM::PPM()
{
	this->mWidth = 0;
	this->mHeight = 0;
	this->mMaxColorValue = 0;
}

int PPM::getHeight( ) const
{
	return this->mHeight;
}

int PPM::getWidth( ) const
{
	return this->mWidth;
}

int PPM::getMaxColorValue( ) const
{
	return this->mMaxColorValue;
}

int PPM::getChannel( const int& row, const int& column, const int& channel ) const
{
	if ( row <= this->mHeight-1 && row >= 0 && column <= this->mWidth-1 && column >= 0 && channel <= 2 && channel >= 0)
	{
		int i = (row * mWidth * 3) + (column * 3) + (channel);
		return mChannels[i];
	}
	return -1;
}

void PPM::setHeight( const int& height )
{
	if (height >= 0)
		{
			this->mHeight = height;
			int size = mWidth * mHeight * 3;
			mChannels.resize(size);
		}
}

void PPM::setWidth( const int& width )
{
	if (width >= 0)
		{
			this->mWidth = width;
			int size = mWidth * mHeight * 3;
			mChannels.resize(size);
		}
}

void PPM::setMaxColorValue( const int& max_color_value )
{
	if (max_color_value >= 0 && max_color_value <= 255)
	{
		this->mMaxColorValue = max_color_value;
	}
}

void PPM::setChannel( const int& row, const int& column, const int& channel, const int& value )
{
	if (row >= 0 && row <= this->mHeight-1 && column >= 0 && column <= this->mWidth-1 && channel >= 0 && channel <= 2 && value >= 0 && value <= this->mMaxColorValue)
	{
		int i = ((row * mWidth * 3) + (column * 3) + (channel));
		mChannels[i] = value;
	}
	
}

std::istream& operator>>(std::istream& is, PPM& p)
{
	int  i, j, k, w, h, mcv;
	std::string key = "P6";
	is >> key >> w >> h >> mcv;
	p.setWidth(w);
	p.setHeight(h);
	p.setMaxColorValue(mcv);


	char junk;
	is >> std::noskipws >> junk;

	unsigned char c;
	for (i = 0; i < h; i++)
	{
		for ( j = 0; j < w; j++)
		{
		 	for ( k = 0; k < 3; k++)
		 	{
		 		is.read((char*) &c, 1);
		 		p.setChannel(i,j,k,c);
		 	}
		}
	}
	return is;
}

std::ostream& operator<<(std::ostream& os, const PPM& p)
{
	int  i, j, k, w, h, mcv;
	std::string key = "P6";
	w = p.getWidth();
	h = p.getHeight();
	mcv = p.getMaxColorValue();
	os << key << " " << w << " " << h << " " << mcv << '\n';


	unsigned char c;
	for (i = 0; i < h; i++)
	{
		for ( j = 0; j < w; j++)
		{
		 	for ( k = 0; k < 3; k++)
		 	{
		 		c = p.getChannel(i,j,k);
		 		os.write((char*) &c, 1);
		 	}
		}
	}
	return os;
}

bool PPM::operator>( const PPM& p) const
{
	return this->getHeight()*this->getWidth() > p.getWidth()*p.getHeight();
}

bool PPM::operator==( const PPM& p) const
{
	return this->getHeight()*this->getWidth() == p.getWidth()*p.getHeight();
}

bool PPM::operator!=( const PPM& p) const
{
	return this->getHeight()*this->getWidth() != p.getWidth()*p.getHeight();
}

bool PPM::operator<( const PPM& p) const
{
	return this->getHeight()*this->getWidth() < p.getWidth()*p.getHeight();
}

bool PPM::operator<=( const PPM& p) const
{
    return this->getHeight()*this->getWidth() <= p.getWidth()*p.getHeight();
}

bool PPM::operator>=( const PPM& p) const
{
    return this->getHeight()*this->getWidth() >= p.getWidth()*p.getHeight();
}

PPM& PPM::operator+=( const PPM& p)
{
	int i,j,k;
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <= 2; k++)
			{
				int new_value = this->getChannel(i,j,k) + p.getChannel(i,j,k);
				
				if (new_value > 255)
				{
					this->setChannel(i,j,k,this->mMaxColorValue);
				}
			this->setChannel(i,j,k,new_value);
			}
		}
	}
	return *this;
}

PPM& PPM::operator-=( const PPM& p)
{
	int i,j,k;
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <= 2; k++)
			{
				int new_value = this->getChannel(i,j,k) - p.getChannel(i,j,k);
				
				if (new_value < 0)
				{
					this->setChannel(i,j,k, 0);
				}
			this->setChannel(i,j,k, new_value);
			}
		}
	}
	return *this;
}

PPM PPM::operator+( const PPM& rhs) const
{
	int i,j,k;
	PPM new_ppm = rhs;
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <= 2; k++)
			{
				int new_value = this->getChannel(i,j,k) + rhs.getChannel(i,j,k);
				
				if (new_value > 255)
				{
					new_ppm.setChannel(i,j,k, this->mMaxColorValue);
				}
			new_ppm.setChannel(i,j,k, new_value);
			}
		}
	}
	return new_ppm;
}

PPM PPM::operator-( const PPM& rhs) const
{
	int i,j,k;
	PPM new_ppm = rhs;
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <= 2; k++)
			{
				int new_value = this->getChannel(i,j,k) - rhs.getChannel(i,j,k);
				
				if (new_value < 0)
				{
					new_ppm.setChannel(i,j,k, 0);
				}
			new_ppm.setChannel(i,j,k, new_value);
			}
		}
	}
	return new_ppm;	
}

PPM& PPM::operator*=( const double& rhs)
{
	int i,j,k;
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <= 2; k++)
			{	
				double new_value = static_cast<double>(this->getChannel(i,j,k)) * rhs;
				int	newer_value = static_cast<int>(new_value);
				
				if (newer_value < 0)
				{
					this->setChannel(i,j,k, 0);
				}
				else if (newer_value > 255)
				{
					this->setChannel(i,j,k,this->mMaxColorValue);
				}	
			this->setChannel(i,j,k, newer_value);
			}
		}
	}
	return *this;
}

PPM& PPM::operator/=( const double& rhs)
{
	int i,j,k;
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j <this->mWidth; j++)
		{
			for (k = 0; k <= 2; k++)
			{
				double new_value = static_cast<double>(this->getChannel(i,j,k)) / rhs;
				int newer_value = static_cast<int>(new_value);
				if (new_value < 0)
				{
					this->setChannel(i,j,k, 0);
				}
				else if (new_value > 255)
				{
					this->setChannel(i,j,k,this->mMaxColorValue);
				}	
			this->setChannel(i,j,k, newer_value);
			}
		}
	}
	return *this;
}

PPM PPM::operator*( const double& rhs) const
{
	int i,j,k;
	PPM new_ppm;
	new_ppm.setHeight(mHeight);	
	new_ppm.setWidth(mWidth);
	new_ppm.setMaxColorValue(mMaxColorValue);
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <= 2; k++)
			{
				double new_value = static_cast<double>(this->getChannel(i,j,k)) * rhs;
				int newer_value = static_cast<int>(new_value);
				if (newer_value > 255)
				{
					new_ppm.setChannel(i,j,k, this->mMaxColorValue);
				}
				else if (newer_value < 0)
				{
					new_ppm.setChannel(i,j,k,0);
				}
			new_ppm.setChannel(i,j,k, newer_value);
			}
		}
	}
	return new_ppm;
}

PPM PPM::operator/( const double& rhs) const
{
	int i,j,k;
	PPM new_ppm;
	new_ppm.setHeight(mHeight);	
	new_ppm.setWidth(mWidth);
	new_ppm.setMaxColorValue(mMaxColorValue);
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <= 2; k++)
			{
				double new_value = static_cast<double>(this->getChannel(i,j,k)) / rhs;
				int newer_value = static_cast<int>(new_value);
				if (newer_value > 255)
				{
					new_ppm.setChannel(i,j,k, this->mMaxColorValue);
				}
				else if (newer_value < 0)
				{
					new_ppm.setChannel(i,j,k,0);
				}
			new_ppm.setChannel(i,j,k, newer_value);
			}
		}
	}
	return new_ppm;
}
PPM PPM::copy(const PPM& p)
{                     
	int i,j,k;
	int copyh = p.getHeight();
	int copyw = p.getWidth();
	int copymcv = p.getMaxColorValue();
	
	this->setHeight(copyh);	
	this->setWidth(copyw);
	this->setMaxColorValue(copymcv);
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <=2; k++)
			{
				int copyvalue = p.getChannel(i,j,k);
				this->setChannel(i,j,k,copyvalue);

			}
		}
	}
	return *this;

}
PPM PPM::red(const PPM& p)
{                     
	int i,j,k;
	int copyvalue;
	int copyh = p.getHeight();
	int copyw = p.getWidth();
	int copymcv = p.getMaxColorValue();
	
	this->setHeight(copyh);	
	this->setWidth(copyw);
	this->setMaxColorValue(copymcv);
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <=2; k++)
			{
				if (k == 0)
				{
					copyvalue = p.getChannel(i,j,k);
				}
				this->setChannel(i,j,k,copyvalue);
				
			}
		}
	}
	return *this;

}
PPM PPM::blue(const PPM& p)
{                     
	int i,j,k;
	int copyvalue;
	int copyh = p.getHeight();
	int copyw = p.getWidth();
	int copymcv = p.getMaxColorValue();
	
	this->setHeight(copyh);	
	this->setWidth(copyw);
	this->setMaxColorValue(copymcv);
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <=2; k++)
			{
				if (k == 1)
				{
					copyvalue = p.getChannel(i,j,k);
				}
				this->setChannel(i,j,k,copyvalue);
				
			}
		}
	}
	return *this;

}
PPM PPM::green(const PPM& p)
{                     
	int i,j,k;
	int copyvalue;
	int copyh = p.getHeight();
	int copyw = p.getWidth();
	int copymcv = p.getMaxColorValue();
	
	this->setHeight(copyh);	
	this->setWidth(copyw);
	this->setMaxColorValue(copymcv);
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			for (k = 0; k <=2; k++)
			{
				if (k == 2)
				{
					copyvalue = p.getChannel(i,j,k);
				}
				this->setChannel(i,j,k,copyvalue);
				
			}
		}
	}
	return *this;

}
PPM PPM::linear(const PPM& p)
{                     
	int i,j;
	double r,g,b,v;
	int copyh = p.getHeight();
	int copyw = p.getWidth();
	int copymcv = p.getMaxColorValue();
	
	this->setHeight(copyh);	
	this->setWidth(copyw);
	this->setMaxColorValue(copymcv);
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			r = p.getChannel(i,j,0);
			g = p.getChannel(i,j,1);
			b = p.getChannel(i,j,2);
			v = 0.2126*r + 0.7152*g + 0.0722*b;
			this->setChannel(i,j,0,(int)v);
			this->setChannel(i,j,1,(int)v);
			this->setChannel(i,j,2,(int)v);
			
		}
	}
	return *this;

}

PPM PPM::vertical(const PPM& p)
{                     
	int i,j;
	int whiteval = 255;
	int blackval = 0;

	int copyh = p.getHeight();
	int copyw = p.getWidth();
	int copymcv = p.getMaxColorValue();
	double r,g,b,v, cutoff,rl,gl,bl,vl;

	cutoff = copymcv * 0.1;
	
	this->setHeight(copyh);	
	this->setWidth(copyw);
	this->setMaxColorValue(copymcv);
	for (i = 0; i < this->mHeight; i++)
	{
		for (j = 1; j < this->mWidth; j++)
		{
			r = p.getChannel(i,j,0);
			g = p.getChannel(i,j,1);
			b = p.getChannel(i,j,2);
			v = 0.2126*r + 0.7152*g + 0.0722*b;

			rl = p.getChannel(i,j-1,0);
			gl = p.getChannel(i,j-1,1);
			bl = p.getChannel(i,j-1,2);
			vl = 0.2126*rl + 0.7152*gl + 0.0722*bl;
			
			if (v < vl - cutoff || v > vl + cutoff)
			{
				this->setChannel(i,j-1,0,whiteval);
				this->setChannel(i,j-1,1,whiteval);
				this->setChannel(i,j-1,2,whiteval);
				
			}
			else 
			{
				
				this->setChannel(i,j-1,0,blackval);
				this->setChannel(i,j-1,1,blackval);
				this->setChannel(i,j-1,2,blackval);
			}
		}
	}
	return *this;

}
PPM PPM::horizontal(const PPM& p)
{                     
	int i,j;
	int whiteval = 255;
	int blackval = 0;
	int copyh = p.getHeight();
	int copyw = p.getWidth();
	int copymcv = p.getMaxColorValue();
	double r,g,b,v, cutoff,ru,gu,bu,vu;

	cutoff = copymcv * 0.1;
	
	this->setHeight(copyh);	
	this->setWidth(copyw);
	this->setMaxColorValue(copymcv);
	for (i = 1; i < this->mHeight; i++)
	{
		for (j = 0; j < this->mWidth; j++)
		{
			r = p.getChannel(i,j,0);
			g = p.getChannel(i,j,1);
			b = p.getChannel(i,j,2);
			v = 0.2126*r + 0.7152*g + 0.0722*b;

			ru = p.getChannel(i-1,j,0);
			gu = p.getChannel(i-1,j,1);
			bu = p.getChannel(i-1,j,2);
			vu = 0.2126*ru + 0.7152*gu + 0.0722*bu;
			
			if (v < vu - cutoff || v > vu + cutoff)
			{
				this->setChannel(i-1,j,0,whiteval);
				this->setChannel(i-1,j,1,whiteval);
				this->setChannel(i-1,j,2,whiteval);
				
			}
			else 
			{
				
				this->setChannel(i-1,j,0,blackval);
				this->setChannel(i-1,j,1,blackval);
				this->setChannel(i-1,j,2,blackval);
			}
		}
	}
	return *this;

}

void PPM::sepiaFilter( PPM& dst ) const
{
	int i,j;
    int copyh = this->getHeight();
	int copyw = this->getWidth();
	int copymcv = this->getMaxColorValue();
	
	dst.setHeight(copyh);
	dst.setWidth(copyw);
	dst.setMaxColorValue(copymcv);
	
	for (i = 0; i < copyh; i++)
	{
		for (j = 0; j < copyw; j++)
		{
			double copyred = this->getChannel(i,j,0)  * 0.393 + this->getChannel(i,j,1) * 0.769 + this->getChannel(i,j,2) * 0.189;
			if ( copyred > copymcv)
			{
				copyred = copymcv;
				dst.setChannel(i,j,0,copyred);
			}
			dst.setChannel(i,j,0,copyred);
			double copygreen = this->getChannel(i,j,0)  * 0.349 + this->getChannel(i,j,1) * 0.686 + this->getChannel(i,j,2) * 0.168;
			if ( copygreen > copymcv)
			{
				copygreen = copymcv;
				dst.setChannel(i,j,1,copygreen);
			}
			dst.setChannel(i,j,1,copygreen);
			double copyblue = this->getChannel(i,j,0)  * 0.272 + this->getChannel(i,j,1) * 0.534 + this->getChannel(i,j,2) * 0.131;
			if ( copyblue > copymcv)
			{
				copyblue = copymcv;
				dst.setChannel(i,j,2,copyblue);
			}
			dst.setChannel(i,j,2,copyblue);			
		}
	}
}

PPM createTestPattern( const int width, const int height, const int max_color_value )
{
	int i,j,k;
	
	PPM p;
	
	p.setHeight(height);
	p.setWidth(width);
	p.setMaxColorValue(max_color_value);
	
	for (i = 0; i < p.getHeight()/2; i++)
	{
		for (j = 0; j < p.getWidth()/2; j++)
		{
			p.setChannel(i,j,0,max_color_value);
		}
	}
	
	for (i = 0; i < p.getHeight()/2; i++)
	{
		for (j = p.getWidth()/2; j < p.getWidth(); j++)
		{
			
			p.setChannel(i,j,1,max_color_value);
		}

	}
	
	for (i = p.getHeight()/2; i < p.getHeight(); i++)
	{
		for (j = 0; j < p.getWidth()/2; j++)
		{

			p.setChannel(i,j,2,max_color_value);
		}
	}
	
	
	for (i = p.getHeight()/2; i < p.getHeight(); i++)
	{
		for (j = p.getWidth()/2; j < p.getWidth(); j++)
		{
			for (k = 0; k <= 2; k++ )
			{
				p.setChannel(i,j,k,max_color_value/2);
			}
		}
	}
	return p;
}

