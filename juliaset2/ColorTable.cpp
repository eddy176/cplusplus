#include "ColorTable.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <stdlib.h>
#include<typeinfo>


Color::Color( )
{
    this->mRed = 0;
    this->mBlue = 0;
    this->mGreen = 0;
}

Color::Color( const int& red, const int& green, const int& blue )
{
    this->mRed = red;
    this->mBlue = blue;
    this->mGreen = green;
}

int Color::getRed( ) const
{
    return this->mRed;
}

int Color::getGreen( ) const
{
    return this->mGreen;
}

int Color::getBlue( ) const
{
    return this->mBlue;
}

int Color::getChannel( const int& channel ) const
{
    if( channel >= 0 && channel <= 2 )
    {
        if( channel == 0 )
        {
            return this->mRed;
        }
        else if( channel == 1 )
        {
            return this->mGreen;
        }
        else
        {
            return this->mBlue;
        }
    }
    return -1;

}

void Color::setRed( const int& value )
{
    if( value >= 0 )
    {
        this->mRed = value;
    }
}

void Color::setGreen( const int& value )
{
    if( value >= 0 )
    {
        this->mGreen = value;
    }
}

void Color::setBlue( const int& value )
{
        if( value >= 0 )
    {
        this->mBlue = value;
    }
}

void Color::setChannel( const int& channel, const int& value )
{
        if( value >= 0 && channel >= 0 && channel <= 2 )
    {
        if( channel == 0 )
        {
            this->mRed = value;
        }
        else if( channel == 1 )
        {
            this->mGreen = value;
        }
        else
        {
            this->mBlue = value;
        }
    }
}

void Color::invert( const int& max_color_value )
{
    if( max_color_value >= mRed && max_color_value >= mGreen && max_color_value >= mBlue )
    {
        this->mRed = max_color_value - mRed;
        this->mBlue = max_color_value - mBlue;
        this->mGreen = max_color_value - mGreen;
    }
}

bool Color::operator==( const Color& rhs ) const
{
    if( mRed == rhs.getChannel( 0 ) && mGreen == rhs.getChannel( 1 ) && mBlue == rhs.getChannel( 2 ))
    {
        return true;
    }
    return false;
}

ColorTable::ColorTable( const size_t& num_color )
    :   mColors(num_color) {

}

size_t ColorTable::getNumberOfColors( ) const
{
    size_t count, i;
    count = 0;
    for( i = 0; i < mColors.size(); i++ )
    {

        count += 1;

    }

    return count;

}

void ColorTable::setNumberOfColors( const size_t& num_color )
{
    mColors.resize(num_color);

}

const Color& ColorTable::operator[]( const int& i ) const
{
     if( i >= 0 && (unsigned)i <= mColors.size() - 1 )
     {
         return mColors[i];
     }
    static Color ec( -1, -1, -1 );
    static Color c( -1, -1, -1 );
    c = ec;
    return c;
}

Color& ColorTable::operator[]( const int& i )
{
    if( i >= 0 && (unsigned)i <= mColors.size() - 1 )
    {
        return mColors[i];
    }
    static Color ec( -1, -1, -1 );
    static Color c( -1, -1, -1 );
    c = ec;
    return c;
}

void ColorTable::setRandomColor( const int& max_color_value, const size_t& position )
{
    if( position < mColors.size() && max_color_value > 0 )
    {
        int rvalue = (rand() %  max_color_value + 1);
        int gvalue = (rand() %  max_color_value + 1);
        int bvalue = (rand() %  max_color_value + 1);
        mColors[position].setChannel( 0 , rvalue );
        mColors[position].setChannel( 1 , gvalue );
        mColors[position].setChannel( 2 , bvalue );

    }
    else if(position < mColors.size() && max_color_value == 0 )
    {
        int rvalue = 0;
        int gvalue = 0;
        int bvalue = 0;
        mColors[position].setChannel( 0 , rvalue );
        mColors[position].setChannel( 1 , gvalue );
        mColors[position].setChannel( 2 , bvalue );
    }
}

void ColorTable::insertGradient( const Color& color1, const Color& color2, const size_t& position1, const size_t& position2 )
{

    if( position1 < mColors.size() && position2 < mColors.size() && position1 < position2 )
    {
        double n = position2 - position1 + 1;


        double red = ( color2.getRed() - color1.getRed() ) /  ( n -  1 );
        double green = ( color2.getGreen() - color1.getGreen() ) /  (n -  1 );
        double blue = ( color2.getBlue() - color1.getBlue() ) /  ( n -  1 );

        for( unsigned int i = 0; i <= n - 1; i++ )
        {
            double redi = color1.getRed() + (double)i * red;
            mColors[position1 + i].setChannel( 0, redi );

            double greeni = color1.getGreen() + (double)i * green;
            mColors[position1 + i].setChannel( 1, greeni );

            double bluei = color1.getBlue() + (double)i * blue;
            mColors[position1 + i].setChannel( 2, bluei );

        }
    }
}
