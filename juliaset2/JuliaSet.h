#ifndef JULIASET_H_
#define JULIASET_H_
#include <vector>
#include <string>

#include"PPM.h"
#include"ColorTable.h"

class JuliaSet
{
    
    public:
        JuliaSet();
 
        int getWidth() const;
        int getHeight() const;
        double getMinX() const;
        double getMaxX() const;
        double getMinY() const;
        double getMaxY() const;
        double getA() const;
        double getB() const;
        int getMaxEscapeCount() const; 

        void setPixelSize( const int& width, const int& height );
        void setPlaneSize( const double& min_x, const double& max_x, const double& min_y, const double& max_y );
        void setParameters( const double& a, const double& b );
        void setMaxEscapeCount( const int& count );

        double getDeltaX( ) const;
        double getDeltaY( ) const;
        void setDeltas( const double& delta_x, const double& delta_y );
        double calculateDeltaX( ) const;
        double calculateDeltaY( ) const;
        double calculatePlaneXFromPixelColumn( const int& column ) const;
        double calculatePlaneYFromPixelRow( const int& row ) const;
        void calculatePlaneCoordinatesFromPixelCoordinates( const int& row, const int& column, double& x, double& y ) const;

        const std::vector< int >& getEscapeCounts( ) const;
        int getEscapeCount( const int& row, const int& column ) const;
        void setEscapeCount( const int& row, const int& column, const int& count );
        void calculateNextPoint( const double x0, const double y0, double& x1, double &y1 ) const; 
        int calculatePlaneEscapeCount( const double& x0, const double& y0 ) const;
        int calculatePixelEscapeCount( const int& row, const int& column ) const;
        void calculateAllEscapeCounts( );

        void setPPM( PPM& ppm, const ColorTable& colors ) const;
        










    protected:
        std::vector<int> mEscapeCounts;
	    int mWidth, mHeight, mMaxEscapeCount;
        double mA, mB, mMinX, mMinY, mMaxX, mMaxY, mDeltaX, mDeltaY;

};


#endif

