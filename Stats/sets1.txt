mysql> use spj;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> (select city from s ) union (select city from j);
--------------
(select city from s ) union (select city from j)
--------------

+--------+
| city   |
+--------+
| London |
| Paris  |
| Athens |
| Rome   |
| Oslo   |
+--------+
5 rows in set (0.00 sec)

mysql> (select city from s ) union (select city from j) union (select city from p);
--------------
(select city from s ) union (select city from j) union (select city from p)
--------------

+--------+
| city   |
+--------+
| London |
| Paris  |
| Athens |
| Rome   |
| Oslo   |
+--------+
5 rows in set (0.00 sec)

mysql> select distinct s.city 
    -> from s 
    -> inner join j
    -> on s.city=j.city;
--------------
select distinct s.city 
from s 
inner join j
on s.city=j.city
--------------

+--------+
| city   |
+--------+
| Paris  |
| Athens |
| London |
+--------+
3 rows in set (0.00 sec)

mysql> select distinct s.city 
    -> from s 
    -> inner join j
    -> on s.city=j.city
    -> inner join p 
    -> on s.city=p.city;
--------------
select distinct s.city 
from s 
inner join j
on s.city=j.city
inner join p 
on s.city=p.city
--------------

+--------+
| city   |
+--------+
| Paris  |
| London |
+--------+
2 rows in set (0.00 sec)

mysql> (select city from s where not exists (select * from p where p.city = s.city)) 
    -> union 
    -> (select city from j where not exists (select * from p where p.city = j.city));
--------------
(select city from s where not exists (select * from p where p.city = s.city)) 
union 
(select city from j where not exists (select * from p where p.city = j.city))
--------------

+--------+
| city   |
+--------+
| Athens |
| Oslo   |
+--------+
2 rows in set (0.00 sec)

mysql> 
