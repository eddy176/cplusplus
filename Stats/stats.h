#ifndef STATS_H_
#define STATS_H_
#include <vector>
#include <iostream>



std::vector<double> getInput(std::istream& input_stream);

double calculateSum(const std::vector <double>& nums);

size_t calculateCount(const std::vector <double>& nums);

double calculateAverage(const std::vector <double>& nums);

double calculateMedian(const std::vector <double>& nums);

double calculateMinimum(const std::vector <double>& nums);

double calculateMaximum(const std::vector <double>& nums);

double calculateCenter( const std::vector< double >& numbers );









#endif
