#include"stats.h"
#include<iostream>
#include<vector>
int main()
{
	int min;
	int max;
	std::vector<double> nums = getInput(std::cin);

	if (nums.size() == 0)
	{
		return 0;
	}

	std::cout << "Sum: " << calculateSum(nums) << std::endl;
	
	std::cout << "Count: " << calculateCount(nums) << std::endl;
	
	std::cout << "Average: " << calculateAverage(nums) << std::endl;

	std::cout << "Median: " << calculateMedian(nums) << std::endl;

	std::cout << " Minimum: " << calculateMinimum(nums) << std::endl;

	std::cout << "Maximum: " << calculateMaximum(nums) << std::endl;

	return 0;

}