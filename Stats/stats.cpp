
#include <algorithm>
#include <iostream>
#include <vector>
#include "stats.h"
#include <typeinfo>


std::vector<double> getInput(std::istream& input_stream)
{
	std::vector<double> nums;
	double x;
	while ( input_stream >> x)
	{
		if ( typeid(x) == typeid(double))
		{
			nums.push_back(x);
		}
		else if ( typeid(x) != typeid(double))
		{
		    return nums;
		}
	}
	return nums;
}

double calculateSum(const std::vector <double>& nums)
{
	double sum = 0;

	for (size_t i = 0; i < nums.size(); i++)
	{
		sum += nums[i];
	}
	return sum;

}

size_t calculateCount(const std::vector <double>& nums)
{
	size_t count = 0;
	
	
	for (size_t i = 0; i < nums.size(); i++)
	{
		count += 1;
	}
	return count;

}

double calculateAverage(const std::vector <double>& nums)
{
	double sum = 0;
	int count = 0;
	double average;
	
	if (nums.size() == 0)
	{
	    return 0.0;
	}
	for (size_t i = 0; i < nums.size(); i++)
	{
		sum += nums[i];
		count += 1;
	}
	average = sum /count;
	return average;
}

double calculateMedian(const std::vector <double>& nums)

{
  double median;
  std::vector < double > copy = nums;
  size_t size = nums.size();


  if (nums.size() == 0)
	{
	    return 0.0;
	}
  std::sort(copy.begin(), copy.end());

  if (size  % 2 == 0)
  {
      median = (copy[size / 2 - 1] + copy[size / 2]) / 2;
  }
  else 
  {
      median = copy[size / 2];
  }

  return median;
}

double calculateMinimum(const std::vector <double>& nums)
{
	if (nums.size() == 0)
	{
	    return 0.0;
	}
	double min = nums[0];

	for (size_t i = 1; i < nums.size(); i++)
	{
		if (nums[i] < min)
		{
			min = nums[i];
		}
	}
	return min;
}

double calculateMaximum(const std::vector <double>& nums)
{
	if (nums.size() == 0)
	{
	    return 0.0;
	}
	double max = nums[0];

	for (size_t i = 1; i < nums.size(); i++)
	{

		if (nums[i] > max)
		{
			max = nums[i];

		}
	}
	return max;
}

double calculateCenter( const std::vector< double >& numbers )
{
	if (numbers.size() == 0)
	{
	    return 0.0;
	}
	double max = numbers[0];

	for (size_t i = 1; i < numbers.size(); i++)
	{

		if (numbers[i] > max)
		{
			max = numbers[i];

		}
	}
	double min = numbers[0];

	for (size_t i = 1; i < numbers.size(); i++)
	{
		if (numbers[i] < min)
		{
			min = numbers[i];
		}
	}
	return max+min/2;
}

