#include "ComplexFractal.h"
#include "ColorTable.h"
#include "JuliaSet.h"
#include "PPM.h"
#include "MandelbrotSet.h"

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sstream>
#include <string>
#include <fstream>
#include <chrono>


void resize_color_table(ColorTable* ct, const char *arg ) 
{
    std::stringstream ss;
    int size;
    ss.str( arg );
    ss >> size;
    if( ss );
    {
        
        if( ct != 0 )
        {
            ct->setNumberOfColors( size );
        }
    }
}

void assign_color( ColorTable* ct, std::string s) {
    
    std::string delimiter1 = ":";
    std::string delimiter2 = ",";

    size_t pos = 0;
    std::string token;
    
    size_t position;
    int r;
    int g;
    int b;
    while ((pos = s.find(delimiter1)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);      
        ss >> position;
        s.erase(0, pos + delimiter1.length());
    }
    if((pos = s.find(delimiter2)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> r;
        s.erase(0, pos + delimiter2.length());
    }
    if ((pos = s.find(delimiter2)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> g;
        s.erase(0, pos + delimiter2.length());
    }
    
    std::stringstream ss;
    ss.str(s);
    ss >> b;
    s.erase(0, pos + delimiter2.length());
    Color c( r, g, b );
    (*ct)[ position ] = c;
}

void assign_random_color(ColorTable* ct, std::string s ) {
    size_t position;
    int max;
    std::string delimiter1 = ":";
    size_t pos = 0;
    std::string token;
    if ((pos = s.find(delimiter1)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> position;
        s.erase(0, pos + delimiter1.length());
    }
    std::stringstream ss;
    ss.str(s);
    ss >> max;
    s.erase(0, pos + delimiter1.length());
    

    ct->setRandomColor( max, position );
}

void assign_color_gradient( ColorTable* ct, std::string s ) {
    size_t position1, position2;
    int r1, g1, b1, r2, g2, b2;
    std::string delimiter1 = ":";
    std::string delimiter2 = ",";
    size_t pos = 0;
    std::string token;
    
    if ((pos = s.find(delimiter1)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> position1;
        s.erase(0, pos + delimiter1.length());
    }
    if ((pos = s.find(delimiter2)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> r1;
        s.erase(0, pos + delimiter2.length());
    }
    if ((pos = s.find(delimiter2)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> g1;
        s.erase(0, pos + delimiter2.length());
    }
    if ((pos = s.find(delimiter1)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> b1;
        s.erase(0, pos + delimiter1.length());
    }
    if ((pos = s.find(delimiter1)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> position2;
        s.erase(0, pos + delimiter1.length());
    }
    if ((pos = s.find(delimiter2)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> r2;
        s.erase(0, pos + delimiter2.length());
    }
    if ((pos = s.find(delimiter2)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> g2;
        s.erase(0, pos + delimiter2.length());
    }
    std::stringstream ss;
    ss.str(token);
    ss >> b2;
    s.erase(0, pos + delimiter2.length());

    Color c1(r1, g1, b1);
    Color c2(r2, g2, b2);
    ct->insertGradient(c1, c2, position1, position2);
}
void set_plane_size(ComplexFractal* FS, std::string s ) {
    double x,X,y,Y;
    std::string delimiter1 = ":";
    std::string delimiter2 = ",";
    size_t pos = 0;
    std::string token;

    if ((pos = s.find(delimiter1)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> x;
        s.erase(0, pos + delimiter1.length());
    }
    if ((pos = s.find(delimiter2)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> X;
        s.erase(0, pos + delimiter2.length());
    }
    if ((pos = s.find(delimiter1)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> y;
        s.erase(0, pos + delimiter1.length());
    }
    std::stringstream ss;
    ss.str(s);
    ss >> Y;
    s.erase(0, pos + delimiter1.length());

    FS->setPlaneSize( x, X, y, Y );
}
void set_pixel_size(ComplexFractal* FS, std::string s ) {
    int w, h;
    std::string delimiter1 = ",";
    size_t pos = 0;
    std::string token;

    if ((pos = s.find(delimiter1)) != std::string::npos) {
        token = s.substr(0, pos);
        std::stringstream ss;
        ss.str(token);
        ss >> w;
        s.erase(0, pos + delimiter1.length());
    }
    std::stringstream ss;
    ss.str(s);
    ss >> h;
    s.erase(0, pos + delimiter1.length());
    FS->setPixelSize( w, h );
}

void set_maximum_escape_value(ComplexFractal* FS, std::string s) {
    int m;
    std::stringstream ss;
    ss.str(s);
    ss >> m;
    FS->setMaxEscapeCount( m );
}
void set_julia_parameter(JuliaSet* FS, bool isSetJ, std::string s) {
    if(isSetJ){
        double a, b;
        std::string delimiter1 = ",";
        size_t pos = 0;
        std::string token;

        if ((pos = s.find(delimiter1)) != std::string::npos) {
            token = s.substr(0, pos);
            std::stringstream ss;
            ss.str(token);
            ss >> a;
            s.erase(0, pos + delimiter1.length());
        }
        std::stringstream ss;
        ss.str(s);
        ss >> b;
        s.erase(0, pos + delimiter1.length());
        
        FS->setParameters( a, b );
    }
    
}

void manage_ppm(ComplexFractal* FS, ColorTable* ct, PPM* ppm, std::string s){
    FS->calculateAllEscapeCounts( );
    FS->setPPM( *ppm, *ct );
    std::string filename = "file";
    filename = s;
    std::ofstream fout( filename, std::ios::binary );
    if( fout ) {
        fout << (*ppm);
        fout.close( );
    } else {
        std::cout << "Error opening " << filename << std::endl;
    }
}


int main( int argc, char **argv ) 
{
    /* srand with seeds at 100 mirocsecond (us) resolution */
    std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now( );
    std::chrono::high_resolution_clock::duration   dur = now.time_since_epoch( );
    std::chrono::microseconds us = std::chrono::duration_cast< std::chrono::microseconds > ( dur );
    srand( us.count( ) / 100 );
    
    ColorTable ct( 16 );
    Color color1( 0, 255, 0);
    Color color2( 255, 0, 255);

    //positions for insertgradient?
    ct.insertGradient( color1, color2, 0, 52);
    JuliaSet js;
    MandelbrotSet ms;

    int c;
    bool setM = false;
    bool setJ = true;
    ComplexFractal *complexfractal ;
    JuliaSet *Julia = new JuliaSet();
    MandelbrotSet *Mandel = new MandelbrotSet();
    complexfractal =  Julia;
    PPM *ppm = new PPM();
    
    for(int i = 0; i < argc; i++)
    {
        std::string temp = argv[i];
        if(temp =="-M")
        {
            setM = true;
            setJ = false;
            argv[i] = "";
            
        }
        if(temp =="-J")
        {
            setJ = true;
            setM = false;
            argv[i] = "";
            
        }

    }

    if(setM)
        complexfractal = Mandel;
    if(setJ)
        complexfractal = Julia;

    while(1)
    {
        c = getopt (argc, argv, "T:A:R:g:r:p:m:a:f:h:?");
        if(c == -1)
            break;
        switch( c )
        {            
            case 'T':
                resize_color_table( &ct , optarg );
                break;
            case 'A':
                assign_color( &ct , optarg );
                break;
            case 'R':
                assign_random_color(&ct , optarg);
                break;
            case 'g':
                assign_color_gradient(&ct , optarg);
                break;
            case 'r':
                set_plane_size(complexfractal , optarg);
                break;
            case 'p':
                set_pixel_size(complexfractal , optarg);
                break;
            case 'm':
                set_maximum_escape_value(complexfractal , optarg);
                break;
            case 'a':
                set_julia_parameter((JuliaSet*)complexfractal , setJ, optarg);
                break;
            case 'f':
                manage_ppm(complexfractal, &ct, ppm, optarg);
                break;
        }
    }
    
    return 0;
    
}