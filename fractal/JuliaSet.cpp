#include "JuliaSet.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <string>

JuliaSet::JuliaSet()
    : ComplexFractal() {
        
    this->mA = -0.650492;
    this->mB = -0.478235;

}

JuliaSet::~JuliaSet( )
{

}

double JuliaSet::getA() const
{
    return this->mA;
}

double JuliaSet::getB() const
{
    return this->mB;
}

void JuliaSet::setParameters( const double& a, const double& b )
{
    if (a <= 2.0 && a >= -2.0 && b <= 2.0 && b >= -2.0)
    {
        this->mA = a;
        this->mB = b;
    }
}

void JuliaSet::calculateNextPoint( const double x0, const double y0, double& x1, double &y1 ) const
{
    x1 = x0*x0 - y0*y0 + this->mA;
    y1 = 2*x0*y0 + this->mB;
}

int JuliaSet::calculatePlaneEscapeCount( const double& x0, const double& y0 ) const
{
    int iters = 0;
    double distance, x1, y1;
    double xc = x0, yc = y0;
    distance = xc*xc + yc*yc;
    if( distance > 4 )
    {
        return iters;
    }
    while( iters <= this->mMaxEscapeCount - 1 )
    {
        distance = xc*xc + yc*yc;
        if( distance > 4 )
        {
            return iters;
        }
        iters += 1;
        calculateNextPoint(xc, yc, x1, y1);
        xc = x1;
        yc = y1;
        
    }
    return iters;

}

int JuliaSet::calculatePixelEscapeCount( const int& row, const int& column ) const
{
    int iters = 0;
    double x0, y0, xc, yc;
    if ( row >= 0 && row <= mHeight -1 && column >= 0 && column <= mWidth -1 )
    {
        x0 = calculatePlaneXFromPixelColumn( column );
        y0 = calculatePlaneYFromPixelRow( row );
        xc = x0;
        yc = y0;
        iters = calculatePlaneEscapeCount( xc, yc );
        return iters;

    }
    return -1;

}


