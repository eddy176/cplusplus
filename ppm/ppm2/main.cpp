#include"ppm.h"
#include<iostream>
#include <fstream>		


int main()
{
	PPM ppm1;
	PPM ppm2;
	PPM ppm3;
	
	char filename1[64];
	char filename2[64];

	std::cout << "Please enter the name of your first file." << std::endl;
	std::cin >> filename1;


	std::ifstream myfile (filename1);
	if (myfile.is_open())
  	{
  		myfile >> ppm1;
	}
	ppm1 *= 1.3;  
  	myfile.close();

	std::cout << "Please enter the name of your second file." << std::endl;
	std::cin >> filename2;

	std::ifstream myfile2 (filename2);
	if (myfile2.is_open())
  	{
  		myfile2 >> ppm2;
	}
	  //ppm2 - int(50);
  	myfile2.close();

	ppm3 = ppm1 += ppm2;
	

	char newfile1[64];
	std::cout << "What would you like to name the output file?" << std::endl;
	std::cin >> newfile1;

	std::ofstream outfile (newfile1);
	outfile.open(newfile1, std::ofstream::out);
	if (outfile.is_open())
	{
		outfile << ppm3;
	}

	outfile.close();


	return 0;

}