#include "rps.h"
#include <time.h>
#include <iostream>
#include <string>
#include <typeinfo>


void initializeRandomNumbers( )
{
	srand(time(NULL));
}


int getRandomPlayerChoice( )
{
	int rand_num = rand() % 3 + 1;
	return rand_num;
}


int getUserPlayerChoice( std::istream& input_stream, std::ostream& output_stream )
{
	std::string player_choice;
	bool incorrect_value = true;
	int answer;

	while(incorrect_value) 
 	{
 		output_stream << "Choose Rock, Paper, or Scissors" << std::endl;
 		input_stream >> player_choice;

 		if ((player_choice == "Rock") or (player_choice == "rock"))
 		{
 			answer = 1;
 			incorrect_value = false;

 		}
 		else if ((player_choice == "Paper") or (player_choice == "paper"))
 		{
 			answer = 2;
 			incorrect_value = false;
 		}
 		else if ((player_choice == "Scissors") or (player_choice == "scissors"))
 		{
 			answer = 3;
 			incorrect_value = false;
 		}
 		else
 		{ 
 			output_stream << "Invalid data type! Please enter your choice again" << std::endl;
 		}
 	}
 	return answer;
}


int determineWinner( int user_choice, int random_choice )
{
	int user = 11;
	int random_player = 12;
	int tie = 13;

	if ((user_choice == 1 and random_choice == 2) or (user_choice == 2 and random_choice == 3) 
	or (user_choice == 3 and random_choice ==1 ) or (user_choice == 4 && random_choice == 2) 
	or (user_choice == 1 && random_choice == 4))
	{
		
		return random_player;
	}
	else if ((random_choice == 1 and user_choice == 2) 
	or (random_choice == 2 and user_choice == 3) 
	or (random_choice == 3 and user_choice ==1 ) or (random_choice == 4 && user_choice == 2) 
	or (random_choice == 3 && user_choice ==4))
	{
		return user;
	}
	else if (user_choice == random_choice)
	{
		
		return tie;
	}

	else
	{
		return 0;
	}
}


void displayMatchResults( std::ostream& output_stream, int user_choice, int random_choice, int winner ) 
{
	std::string user;
	std::string random;
	
	if (user_choice == 1)
	{
		user = "rock";
	}
	else if (user_choice == 2)
	{
		user = "paper";
	}
	else
	{
		user = "scissors";
	}
	
	if (random_choice == 1)
	{
		random = "rock";
	}
	else if (random_choice == 2)
	{
		random = "paper";
	}
	else
	{
		random = "scissors";
	}
	
	if (winner == 11)
		output_stream << "You chose " << user << ". Computer chose " << random << ". You win." << std::endl;
	else if (winner == 12)
		output_stream << "You chose " << user << ". Computer chose " << random <<  ". You lose." << std::endl;
	else if (winner == 13)
		output_stream << "You chose " << user << ". Computer chose " << random <<  ". You tie." << std::endl;
	else
		output_stream << "Either you or the computer gave an invalid response" << std::endl;
}


void displayStatistics( std::ostream& output_stream, int number_user_wins, int number_user_losses, int number_user_ties )
{
	output_stream << "Win: " << number_user_wins << "  Lose: " << number_user_losses << "  Tie: " << number_user_ties << std::endl;
}


bool askUserIfGameShouldContinue( std::istream& input_stream, std::ostream& output_stream )
{
	output_stream << "Would you like to continue? Y to continue n to quit" << std::endl;
	std::string play;
	input_stream >> play;
	bool playgame;
	if  (play.substr(0,1) == "Y" or play.substr(0,1) == "y")
	{
		playgame = true;

	}
	else
	{
		playgame = false;
	}
	return playgame;
}












