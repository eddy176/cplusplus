#include "rps.h"
#include <iostream>


int main()
{
	int number_user_wins = 0;
	int number_user_losses = 0;
	int number_user_ties = 0;
	bool playing = true;
	initializeRandomNumbers();
	
	while ( playing )
	{
		int user_choice = getUserPlayerChoice(std::cin, std::cout);
		int random_choice = getRandomPlayerChoice();
		int winner = determineWinner(user_choice, random_choice);

		if (winner == 11)
		{
			number_user_wins += 1;
		}
		else if (winner == 12)
		{
			number_user_losses += 1;
		}
		else
		{
			number_user_ties += 1;
		}

		displayMatchResults(std::cout, user_choice, random_choice, winner);
		displayStatistics(std::cout, number_user_wins, number_user_losses, number_user_ties);
		playing = askUserIfGameShouldContinue(std::cin, std::cout);
		
	}
	return 0;
}