# ifndef JULIA_SET_H_
# define JULIA_SET_H_

class JuliaSet
{
public:

	Juliaset();
	JuliaSet(
		int Width, 
		int Height,
		int MaxEsc,
		double minX,
		double maxX,
		double minY,
		double maxY,
		double A, 
		double B
		
	);
	
	
	int getWidth() const;
	int getHeight() const;
	int getMaxEscapeCount() const;
	double getMinX() const;
	double getMaxX() const;
	double getMinY() const;
	double getMaxY() const;
	double getA() const;
	double getB() const;
	

	//setters
	void setPixelSize( const int& width, const int& height );
	void setPlaneSize( const double& minX, const double& maxX, const double& minY, const double& maxY );
	void setParameters( const double& a, const double& b );
	void setMaxEscapeCount( const int& MaxEsc );

	//coordinate methods
	double calculateDeltaX( ) const;
	double calculateDeltaY( ) const;
	double calculatePlaneXFromPixelColumn( const int& column ) const;
	double calculatePlaneYFromPixelRow( const int& row ) const;
	void calculatePlaneCoordinatesFromPixelCoordinates( const int& row, const int& column, double& x, double& y ) const;

	//escape methods
	void calculateNextPoint( const double x0, const double y0, double& x1, double &y1 ) const;
	int calculatePlaneEscapeCount( const double& x0, const double& y0 ) const;
	int calculatePixelEscapeCount( const int& row, const int& column ) const;
	void calculateAllEscapeCounts( );
	int getPixelEscapeCount( const int& row, const int& column ) const;
private:
	int mWidth, 
	int mHeight,
	int mMaxEsc,
	double mMinX,
	double mMaxX,
	double mMinY,
	double mMaxY,
	double mA, 
	double mB

};


#endif
	